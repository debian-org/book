# Minimal Sunucu Nasıl Kurulur
Bu eğitim, birçok ekran görüntüsü ile ayrıntılı olarak Debian 11 ( Bullseye ) minimal sunucunun nasıl kurulacağını gösterir. 

:::{admonition} Ön Not
Debian 11.6 64Bit ( amd64 ) kurulum ortamını kullanılacaktır.
:::
:::{admonition} Uyarı
:class: warning
Bu öğreticide, ana bilgisayar adını olarak ```server1.example.com``` IP adresiyle ***192.168.0.100*** ve ağ geçidi ***192.168.0.1*** Bu ayarlar sizin için farklı olabilir, bu nedenle uygun olan yerlerde değiştirmeniz gerekir.
:::

## 1. Gereksinimler 
Bir Debian 11 sunucu sistemi kurmak için aşağıdakilere ihtiyacınız olacak:

- Debian GNU / Linux işletim sistemi için, 
    - [Donanım için amd64 mimari, ağdan kurulan CD (175 MB) boyutunda kalıp indirilebilir.](https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-11.6.0-amd64-netinst.iso)
    - [Donanım için amd64 mimari, CD (650 MB) boyutunda kalıp indirilebilir.](https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/)
    - [Donanım için amd64 mimari, DVD (4,4 GB) boyutunda kalıp indirilebilir.](https://cdimage.debian.org/debian-cd/current/amd64/iso-dvd/)
- Hızlı bir İnternet bağlantısı.
---
* devam edecek... *
---

